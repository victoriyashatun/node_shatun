const express = require('express');
const router = new express.Router();
const { task: taskController } = require('../controllers');

router.get('/', taskController.list);
router.post('/', taskController.add);
router.put('/:id', taskController.editTodo);
router.put('/', taskController.checkedAll);
router.delete('/:id', taskController.deleteOne);
router.delete('/', taskController.deleteAll);

module.exports = router;
