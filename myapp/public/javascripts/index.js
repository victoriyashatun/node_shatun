$(document).ready(() => {
  const TODO_PER_PAGE = 5;
  const ENTER_KEY = 13;
  const $pagination = $('.pagination');
  const API = 'http://192.168.2.170:3000/todo/';
  const time = 2500;
  const DURATION = 300;

  let currentPage = 1;
  let todoArray = [];
  let filterType = 'AllTodos';

  const charReplace = function(name) {
    const eMap = {
      '"': '&quot;',
      '&': '&amp;',
      '\'': '&#39;',
      '/': '&#x2F;',
      '<': '&lt;',
      '=': '&#x3D;',
      '>': '&gt;',
      '`': '&#x60;',
    };

    return name.replace(/[&<>"'`=/]/gu, el => eMap[el]);
  };

  const filtration = () => {
    let filtrateArray = [];

    if (filterType === 'AllTodos') {
      filtrateArray = todoArray;
    } else if (filterType === 'ShowActiveTodos') {
      filtrateArray = todoArray.filter(item => item.status === false);
    } else if (filterType === 'ShowCompletedTodos') {
      filtrateArray = todoArray.filter(item => item.status === true);
    }

    return filtrateArray;
  };

  const addPages = () => {
    $pagination.empty();
    const numberOfPages = Math.ceil(filtration().length / TODO_PER_PAGE);

    let pages = '';

    for (let i = 1; i <= numberOfPages; i++) {
      pages += `<button class="button-page"
    id="${i}" text="${i}" id="page">${i}</button>`;
    }
    $pagination.append(pages);
  };

  const slicingPerPages = function() {
    const sortFilter = filtration();
    const allPossibleTodo = currentPage * TODO_PER_PAGE;
    const firstTodoPage = allPossibleTodo - TODO_PER_PAGE;
    const lastTodoPage = firstTodoPage + TODO_PER_PAGE;

    return sortFilter.slice(firstTodoPage, lastTodoPage);
  };

  const render = function() {
    $('#todo-roster').empty();
    const sliceArray = slicingPerPages();

    let stringForAppend = '';

    sliceArray.forEach(currentTodo => {
      let todoItemClass = '';

      if (currentTodo.status) {
        todoItemClass = 'todo-item line';
      } else {
        todoItemClass = 'todo-item';
      }
      stringForAppend += `<li id=${currentTodo.id}
class='${todoItemClass}' class="todo-item">
        <div> <input type="checkbox" class="todo-item-checkbox"
${currentTodo.status && 'checked'} /></div>
      <span class="todo-item-value">${currentTodo.text}</span>
        <div><button class="delete-button">Delete</button></div>
          </li>`;
    });

    $('#todo-roster').append(stringForAppend);
    const { length: active } = todoArray.filter(item => item.status === false);
    const { length: complete } = todoArray.filter(item => item.status === true);
    const condition = todoArray.every(item => item.status === true)
            && todoArray.length;

    $('#check-all')
      .prop('checked', condition);
    if (todoArray.length > 0) {
      $('#todo-roster')
        .append(`<hr><p>Active todo: ${active} 
                                 Completed todo: ${complete}</p>`);
    }
    addPages();
  };

  $pagination.on('click', 'button', event => {
    currentPage = $(event.currentTarget).attr('text');
    render();
    $(`#${currentPage}.button-page`).addClass('button-select');
  });
  const showErrors = function() {
    $('.show-errors').show();
    setTimeout(() => {
      $('.show-errors').hide(DURATION);
    }, time);
  };

  const request = async function(method, data, url = '') {
    const requests = await fetch(`${API}${url}`, {
      body: JSON.stringify({ data }),
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      method,
    });

    if (method !== 'DELETE') {
      const returnData = await requests.json();

      return returnData;
    }

    return requests;
  };
  const get = async() => {
    try {
      const response = await fetch(`${API}`);
      const data = await response.json();

      todoArray = data;
      render();
    } catch (error) {
      showErrors();
    }
  };
  const editTodo = async(id, changes) => {
    try {
      await request('PUT', changes, `${id}`);
      get();
    } catch (error) {
      showErrors();
    }
  };

  $(document).ready(get);

  $('#add-button').click(async() => {
    const newValue = $('#todo-input')
      .val()
      .trim();
    const replacedValue = charReplace(newValue);

    if (replacedValue) {
      try {
        const result = await request('POST', replacedValue);

        todoArray.push(result);
        currentPage = Math.ceil(todoArray.length / TODO_PER_PAGE);
        render();
        $(`#${currentPage}.button-page`).addClass('button-select');
      } catch (error) {
        showErrors();
      }
      $('#todo-input')
        .val('');
    }
  });

  $('#todo-input')
    .focus();

  $('#todo-input').on('keyup', event => {
    if (event.which === ENTER_KEY) {
      $('#add-button')
        .click();
    }
  });

  $(document)
    .on('change', '#check-all', async() => {
      try {
        const idCheckAll = Boolean($('#check-all').prop('checked'));

        await request('PUT', idCheckAll);
        todoArray.forEach(el => {
          el.status = idCheckAll;
          currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
        });
        render();
        $(`#${currentPage}.button-page`).addClass('button-select');
      } catch (error) {
        showErrors();
      }
    });

  $(document)
    .on('change', '.todo-item-checkbox', () => {
      const todoID = Number(event.target.offsetParent.id);
      const status = Boolean(event.target.checked);

      editTodo(todoID, { status });
    });

  $(document).on('click', '.delete-button', async function() {
    const todoID = Number(this.parentElement.parentElement.id);

    try {
      await request('DELETE', {}, `${todoID}`);
      todoArray.forEach((el, index) => {
        if (el.id === todoID) {
          todoArray.splice(index, 1);
        }
        const isLastPage = currentPage
          >= Math.ceil(filtration().length / TODO_PER_PAGE);
        const shouldRemove = filtration().length % TODO_PER_PAGE === 0;

        if (isLastPage && shouldRemove) {
          currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
        }
        render();
        $(`#${currentPage}.button-page`).addClass('button-select');
      });
    } catch (error) {
      showErrors();
    }
  });

  $(document)
    .on('click', '#all', () => {
      filterType = 'AllTodos';
      currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
      render();
      $('#active').removeClass('select-tab');
      $('#complete').removeClass('select-tab');
      $('#all').addClass('select-tab');
      $(`#${currentPage}.button-page`).addClass('button-select');
    });

  $(document)
    .on('click', '#complete', () => {
      filterType = 'ShowCompletedTodos';
      currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
      render();
      $('#active').removeClass('select-tab');
      $('#all').removeClass('select-tab');
      $('#complete').addClass('select-tab');
      $(`#${currentPage}.button-page`).addClass('button-select');
    });

  $(document)
    .on('click', '#active', () => {
      filterType = 'ShowActiveTodos';
      currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
      render();
      $('#complete').removeClass('select-tab');
      $('#all').removeClass('select-tab');
      $('#active').addClass('select-tab');
      $(`#${currentPage}.button-page`).addClass('button-select');
    });


  $('#todo-roster')
    .on('dblclick', 'span', event => {
      const title = $(event.currentTarget).text();
      const $thisParent = $(event.currentTarget).parent();

      $thisParent.html(`<input type="text" id="edit" text="">`);
      $thisParent.removeClass('line');
      const $edit = $('#edit');

      $edit.focus();
      $edit.val(title);
    });
  const saveEdit = () => {
    const clickId = Number($(event.target)
      .parent()
      .attr('id'));
    const value = $('#edit')
      .val()
      .trim();

    const replacedValue = charReplace(value);

    if (value) {
      editTodo(clickId, { text: replacedValue });
    }

    return false;
  };

  $('#todo-roster')
    .on('keydown', '#edit', event => {
      if (event.which === ENTER_KEY) {
        saveEdit();
      }
    });
  $('#todo-roster')
    .on('blur', '#edit', saveEdit);

  $(document)
    .on('click', '#delete-all', async() => {
      try {
        await request('DELETE', {});
        todoArray = todoArray.filter(el => el.status === false);
        const isPageOver = currentPage
          > Math.ceil(filtration().length / TODO_PER_PAGE);

        if (isPageOver) {
          currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
        }
        render();
        $(`#${currentPage}.button-page`).addClass('button-select');
      } catch (error) {
        showErrors();
      }
    });
});
