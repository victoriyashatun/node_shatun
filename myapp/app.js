const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const indexRouter = require('./routes/index');
const todoRouter = require('./routes/todo');
const app = express();
const NOT_FOUND = 404;
const INTERNAL_SERVER_ERROR = 500;
const cors = require('cors');


app.use(cors({ origin: '*' }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
app.use('/todo', todoRouter);
app.use((req, res, next) => {
  next(createError(NOT_FOUND));
});


app.use((err, req, res) => {

  // eslint-disable-next-line prefer-destructuring
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};


  res.status(err.status || INTERNAL_SERVER_ERROR);
  res.render('error');
});

module.exports = app;
