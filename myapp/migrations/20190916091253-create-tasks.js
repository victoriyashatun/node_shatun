'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('tasks', {
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    status: { type: Sequelize.BOOLEAN, defaultValue: false },
    text: { type: Sequelize.STRING },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  }),
  down: queryInterface => queryInterface.dropTable('tasks'),
};
