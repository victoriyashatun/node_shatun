'use strict';

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('task', {
    text: DataTypes.STRING,
    status: DataTypes.BOOLEAN,
  }, {});
};
