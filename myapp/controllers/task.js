const { task } = require('../models');
const OK = 200;
const NOT_CONTENT = 204;
const BAD_REQUEST = 400;

const list = async function(req, res) {
  try {
    const data = await task.findAll({ order: [['id', 'ASC']] });

    return res.status(OK).send(data);
  } catch (error) {
    return res.status(BAD_REQUEST).send(error);
  }
};

const add = async function(req, res) {
  try {
    const todo = await task.create({ text: req.body.data });

    return res.status(OK).send(todo);
  } catch (error) {
    return res.status(BAD_REQUEST).send(error);
  }
};

const deleteOne = async function(req, res) {
  try {
    const todo = await task.findByPk(req.params.id);

    if (todo) {
      await task.destroy({ where: { id: req.params.id } });

      res.status(NOT_CONTENT).send({ text: 'ok' });
    } else {
      res.status(BAD_REQUEST).send({ message: 'Todo Not Found' });
    }
  } catch (error) {
    res.status(BAD_REQUEST).send(error);
  }
};

const checkedAll = async function(req, res) {
  try {
    const { body: { data: statusAll } } = req;

    await task.update({ status: statusAll }, { where: { status: !statusAll } });

    return res.status(OK).send({});
  } catch (error) {
    return res.status(BAD_REQUEST).send(error);
  }
};

const deleteAll = async function(req, res) {
  try {
    await task.destroy({ where: { status: true } });

    return res.status(OK).send({});
  } catch (error) {
    return res.status(BAD_REQUEST).send(error);
  }
};

const editTodo = async function(req, res) {
  try {
    const { body: { data } } = req;
    const todo = await task.update(data, { where: { id: req.params.id } });

    return res.status(OK).send(todo);
  } catch (error) {
    return res.status(BAD_REQUEST).send(error);
  }
};


module.exports = {
  add,
  checkedAll,
  deleteAll,
  deleteOne,
  editTodo,
  list,
};


